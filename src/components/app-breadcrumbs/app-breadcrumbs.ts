import router from '../../router/router.ts';
export function AppBreadcrumbs(...segments: string[]) {
  let breadcrumbHtml =
    '<li><a href="/" class="breadcrumb-link text-gray-500 hover:text-gray-900">Home</a></li>';

  segments.forEach((segment, index) => {
    const isLastSegment = index === segments.length - 1;
    const segmentName = segment.charAt(0).toUpperCase() + segment.slice(1);

    // If it's the last segment, display it as text. Otherwise, as a link.
    if (isLastSegment) {
      breadcrumbHtml += `<span class="text-gray-500 mx-2">&gt;</span><li><span class="text-gray-700 font-medium">${segmentName}</span></li>`;
    } else {
      const href = generateBreadcrumbLink(segment);
      breadcrumbHtml += `<span class="text-gray-500 mx-2">&gt;</span><li><a href="${href}" class="breadcrumb-link text-gray-500 hover:text-gray-900">${segmentName}</a></li>`;
    }
  });

  return `
    <nav aria-label="breadcrumb" class="breadcrumbs mx-auto max-w-screen-xl pb-6 text-gray-700 text-sm lg:text-base">
      <ul class="list-none p-0 inline-flex capitalize">
        ${breadcrumbHtml}
      </ul>
    </nav>
  `;
}

//Placeholder used
function generateBreadcrumbLink(segment: string) {
  return `/category/${segment.toLowerCase()}`;
}

export function setupBreadcrumbsListener() {
  document
    .querySelector('.breadcrumbs')
    ?.addEventListener('click', breadcrumbsNavigation);
}

function breadcrumbsNavigation(event: Event) {
  event.preventDefault();
  const target = (event.target as Element).closest('a.breadcrumb-link');
  if (target) {
    const href = (target as HTMLAnchorElement).getAttribute('href');
    if (href && href.startsWith('/')) {
      router.navigate(href);
    }
  }
}
