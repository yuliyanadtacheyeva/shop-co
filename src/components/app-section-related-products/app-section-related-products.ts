import { createProductCard } from '../app-card-product/app-card-product.ts';
import { fetchProductsByCategory } from '../../utils/apiUtils';
export async function AppSectionRelatedProducts(
  currentCategory: string,
  limit: number,
  currentProductId: number,
): Promise<string> {
  const relatedProducts = await fetchProductsByCategory(currentCategory, limit);

  // Filter out the current product
  const filteredProducts = relatedProducts.filter(
    (product) => product.id !== currentProductId,
  );

  const productCardsHtml = filteredProducts
    .map((product) => createProductCard(product))
    .join('');

  return `
<section class="related-products mt-16">
  <h3 class="font-poppins text-5xl text-center font-bold mb-10">You might also like</h3>
  <div class="related-products-cards grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
    ${productCardsHtml}
  </div>
  </section>
`;
}
