import { attachProductClickListeners } from '../app-card-product/app-card-product-handlers.ts';

export function initializeRelatedProductsHandlers() {
  attachProductClickListeners('.related-products-cards');
}
