import router from '../../router/router.ts';
import store from '../../redux/store.ts';
import { clearUserData } from '../../redux/slices/userSlice.ts';
import { deleteCart } from '../../redux/slices/cartSlice.ts';

export function initializeHeaderHandlers() {
  const logo = document.querySelector('.logo');
  logo?.addEventListener('click', handleLogoClick);

  const userIconLink = document.querySelector('.user-icon');
  userIconLink?.addEventListener('click', handleUserIconClick);

  const cartIconLink = document.querySelector('.cart-icon');
  cartIconLink?.addEventListener('click', handleCartIconClick);

  updateHeaderUserIcon();
  store.subscribe(updateHeaderUserIcon);
  store.subscribe(updateCartCount);
}

function handleLogoClick(event: Event) {
  event.preventDefault();
  router.navigate('/');
}

function handleUserIconClick(event: Event) {
  event.preventDefault();
  const state = store.getState();
  if (state.user.isLoggedIn) {
    // Mock logout call
    new Promise((resolve) => setTimeout(resolve, 1000)).then(() => {
      store.dispatch(clearUserData());
      store.dispatch(deleteCart());
      router.navigate('/');
    });
  } else {
    router.navigate('/login');
  }
}

function handleCartIconClick(event: Event) {
  event.preventDefault();
  const state = store.getState();
  const cartId = state.cart.id;

  if (cartId) {
    router.navigate(`/cart/${cartId}`);
  } else {
    router.navigate('/cart');
  }
}

function updateHeaderUserIcon() {
  const userIconLink = document.querySelector('.user-icon');
  if (userIconLink) {
    const state = store.getState();
    const tooltipText = state.user.isLoggedIn ? 'Sign out' : 'Log in';
    userIconLink.setAttribute('title', tooltipText);
  }
}

function updateCartCount() {
  const cartCountElement = document.querySelector('.cart-count');
  if (cartCountElement) {
    const state = store.getState();
    const itemCount = state.cart.products.reduce(
      (total, item) => total + item.quantity,
      0,
    );

    if (itemCount > 0) {
      (cartCountElement as HTMLElement).textContent = itemCount.toString();
      (cartCountElement as HTMLElement).style.display = 'flex';
    } else {
      (cartCountElement as HTMLElement).style.display = 'none';
    }
  }
}
