import cartIconSrc from '../../assets/icons/cart.svg';
import userIconSrc from '../../assets/icons/user.svg';

export function AppHeader() {
  const host = document.createElement('header');
  host.classList.add('header', 'sticky', 'top-0', 'bg-white', 'z-50');

  host.innerHTML = `
    <div class="mx-auto max-w-screen-xl flex justify-between items-center px-4 py-5 border-b border-b-gray-200 sm:px-6 lg:px-8">
      <a href="/" class="logo text-3xl text-black font-poppins font-bold hover:text-gray-600 hover:scale-110 transition duration-300">SHOP.CO</a>
      <div class="flex items-center gap-3.5">
<a href="/cart" class="cart-icon relative hover:scale-125 transition duration-300">
  <img src=${cartIconSrc} alt="Cart">
  <span class="cart-count absolute top-[-20px] left-[-15px] text-red-600 font-bold text-lg px-1.5 py-0.5"></span>
</a>
        <a href="/login" class="user-icon hover:scale-125 transition duration-300">
          <img src=${userIconSrc} alt="User">
        </a>
      </div>
    </div>
  `;

  return host;
}
