import store from '../../redux/store.ts';
import router from '../../router/router.ts';
import { setUserLogin } from '../../redux/slices/userSlice.ts';
import { validateInput } from '../../utils/validationUtils.ts';
import { authenticateUser } from '../../utils/apiUtils.ts';

async function handleAuthFormSubmit(event: Event) {
  event.preventDefault();
  const form = event.target as HTMLFormElement;
  let isFormValid = true;

  const inputs = form.querySelectorAll('input');
  inputs.forEach((input) => {
    if (!validateInput(input)) {
      isFormValid = false;
    }
  });

  if (!isFormValid) {
    console.error('Form validation failed');
    return;
  }

  const formData = new FormData(form);
  const username = formData.get('username') as string;
  const password = formData.get('password') as string;
  try {
    const data = await authenticateUser(username, password);
    store.dispatch(
      setUserLogin({ id: data.id, token: data.token, isLoggedIn: true }),
    );
    router.navigate('/');
  } catch (error) {
    console.error('Login error:', error);
  }
}

export function initializeAuthForm() {
  const form = document.getElementById('auth-form') as HTMLFormElement;
  if (form) {
    form.addEventListener('submit', handleAuthFormSubmit);
    form.querySelectorAll('input').forEach((input) => {
      input.addEventListener('input', () => validateInput(input));
    });
  }
}
