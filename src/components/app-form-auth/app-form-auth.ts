export function AppFormAuth(type: 'login' | 'register') {
  const formTitle =
    type === 'login' ? 'Login to Your Account' : 'Register New Account';
  const buttonText = type === 'login' ? 'Login' : 'Register';
  const passwordAutocomplete =
    type === 'login' ? 'current-password' : 'new-password';

  return `
    <section class="bg-white mx-auto max-w-screen-xl px-4">
      <form id="auth-form" action="#" novalidate class="max-w-xl border border-gray-300 rounded-3xl px-8 pt-6 pb-8 mt-20 mb-4 mx-auto">
        <fieldset>
          <legend class="text-center mb-4 text-2xl font-bold">${formTitle}</legend>
          <div class="mb-5 relative">
            <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="username" type="text" id="username" placeholder="Username" required>
            <div id="username-validation" class="text-sm text-red-500 hidden">Please enter a username between 3 and 32 characters long</div>
          </div>
          <div class="mb-5 relative">
            <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="password" type="password" id="password" placeholder="Password" autocomplete="${passwordAutocomplete}" required>
            <div id="password-validation" class="text-sm text-red-500 hidden">Please enter a password between 3 and 32 characters long</div>
          </div>
          <div class="flex items-center justify-between">
            <button class="bg-black hover:bg-gray-700 text-white font-bold py-3 px-4 rounded-full w-full focus:outline-none focus:shadow-outline" type="submit">
              ${buttonText}
            </button>
          </div>
        </fieldset>
      </form>
    </section>
  `;
}
