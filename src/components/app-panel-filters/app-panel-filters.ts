import './app-panel-filters-styles.css';
import filterIconSrc from '../../assets/icons/filter.svg';
import { AppFilterBrands } from '../app-filter-brands/app-filter-brands.ts';

export function AppPanelFilters(brands: string[], sliderId: string): string {
  const brandFiltersHTML = AppFilterBrands(brands);

  return `
    <div class="flex justify-between pb-2 border-b border-b-gray-200">
      <h2 class="text-xl font-bold">Filters</h2>
      <img src="${filterIconSrc}" alt="filter icon">
    </div>
    ${brandFiltersHTML}
    <div class="range-slider-container mt-4 mb-20">
      <h3 class="text-lg font-semibold">Price</h3>
      <div class="range-slider relative m-4">
        <div id="${sliderId}" class="noUi-target noUi-ltr noUi-horizontal"></div>
      </div>
    </div>
    <div class="buttons mt-4">
      <button type="button" class="apply-filter w-full rounded-full bg-black text-white px-4 py-2 hover:bg-gray-700 transition duration-300">Apply Filter</button>
      <button type="button" class="reset-filter w-full rounded-full mt-2 bg-gray-100 text-black px-4 py-2 hover:bg-gray-300 transition duration-300">Reset Filter</button>
    </div>
  `;
}
