import 'nouislider/dist/nouislider.css';
import {
  initializeRangeSlider,
  updateRangeSliderValues,
} from '../app-range-slider/app-range-slider-handlers.ts';
import {
  attachBrandFilterListeners,
  resetBrandFilters,
  updateBrandFilterUI,
} from '../app-filter-brands/app-filter-brands-handlers.ts';
import store from '../../redux/store.ts';
import { applyFilters, resetFilters } from '../../redux/slices/filterSlice.ts';

export function initializeFilterPanel(
  filtersContainerSelector: string,
  sliderId: string,
  minPrice: number,
  maxPrice: number,
) {
  attachBrandFilterListeners(filtersContainerSelector);
  initializeRangeSlider(sliderId, minPrice, maxPrice);
  attachFilterButtonListeners(filtersContainerSelector, sliderId);
  setupUIUpdateSubscriptions(filtersContainerSelector, sliderId);
}

function attachFilterButtonListeners(
  filtersContainerSelector: string,
  sliderId: string,
) {
  const applyButtons = document.querySelectorAll(
    `${filtersContainerSelector} .apply-filter`,
  );
  const resetButtons = document.querySelectorAll(
    `${filtersContainerSelector} .reset-filter`,
  );

  applyButtons.forEach((button) => {
    button.addEventListener('click', () => handleApplyFilters());
  });

  resetButtons.forEach((button) => {
    button.addEventListener('click', () =>
      handleResetFilters(filtersContainerSelector, sliderId),
    );
  });
}

function handleApplyFilters() {
  store.dispatch(applyFilters());
}

export function handleResetFilters(
  filtersContainerSelector: string,
  sliderId: string,
) {
  resetBrandFilters(filtersContainerSelector);
  store.dispatch(resetFilters());
  const initialState = store.getState().filter;
  updateRangeSliderValues(
    sliderId,
    initialState.priceRange.min,
    initialState.priceRange.max,
  );
}

function setupUIUpdateSubscriptions(
  filtersContainerSelector: string,
  sliderId: string,
) {
  let previousSelectedBrands = store.getState().filter.selectedBrands;
  store.subscribe(() => {
    const currentState = store.getState().filter;
    const currentSelectedBrands = currentState.selectedBrands;

    if (previousSelectedBrands !== currentSelectedBrands) {
      updateBrandFilterUI(filtersContainerSelector);
      previousSelectedBrands = currentSelectedBrands;
    }

    updateRangeSliderValues(
      sliderId,
      currentState.priceRange.min,
      currentState.priceRange.max,
    );
  });
}
