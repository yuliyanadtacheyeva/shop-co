import store from '../../redux/store.ts';
import { toggleBrand } from '../../redux/slices/filterSlice.ts';

export function attachBrandFilterListeners(filtersContainerSelector: string) {
  const container = document.querySelector(filtersContainerSelector);
  if (!container) return;

  container.querySelectorAll('.brand-filter').forEach((filter) => {
    filter.addEventListener('click', (event) => brandFilterClickHandler(event));
  });
}

function brandFilterClickHandler(event: Event) {
  const target = event.target as HTMLElement;
  const brand = target.getAttribute('data-brand');

  if (brand) {
    store.dispatch(toggleBrand(brand));
  }
}

export function updateBrandFilterUI(filtersContainerSelector: string) {
  const selectedBrands = store.getState().filter.selectedBrands;

  document
    .querySelectorAll(`${filtersContainerSelector} .brand-filter`)
    .forEach((element) => {
      const brand = element.getAttribute('data-brand');
      if (brand && selectedBrands.includes(brand)) {
        element.classList.add('font-bold');
      } else {
        element.classList.remove('font-bold');
      }
    });
}

export function resetBrandFilters(filtersContainerSelector: string) {
  document
    .querySelectorAll(`${filtersContainerSelector} .brand-filter`)
    .forEach((element) => {
      element.classList.remove('font-bold');
    });
}
