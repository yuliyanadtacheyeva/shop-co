import store from '../../redux/store.ts';

export function AppFilterBrands(brands: string[]): string {
  const selectedBrands = store.getState().filter.selectedBrands;

  const brandFilters = brands
    .map((brand) => {
      const isSelected = selectedBrands.includes(brand);
      const brandClass = isSelected
        ? 'brand-filter cursor-pointer mb-2 font-bold'
        : 'brand-filter cursor-pointer mb-2';
      return `<div class="${brandClass}" data-brand="${brand}">${brand}</div>`;
    })
    .join('');

  return `
    <div class="brands mt-4 pb-2 border-b border-b-gray-200">
      <h3 class="text-lg font-semibold mb-2">Brand</h3>
      ${brandFilters}
    </div>
  `;
}
