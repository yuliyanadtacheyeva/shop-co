import heroImgSrc from '../../assets/images/hero-img.png';
import starLSrc from '../../assets/icons/hero-star-l.svg';
import starSSrc from '../../assets/icons/hero-star-s.svg';

export function AppHero() {
  return `
        <section class="hero bg-[#F2F0F1]">
        <div
          class="container mx-auto max-w-screen-xl px-4 md:px-6 lg:px-8 space-y-8 sm:px-6 lg:space-y-16 flex flex-col lg:flex-row items-center"
        >
          <div
            class="flex-1 flex flex-col justify-center px-4 lg:px-0 py-10 lg:py-0"
          >
            <h1 class="text-6xl font-bold text-gray-800">
              FIND <span class="underline">ANYTHING</span> THAT MATCHES YOUR
              STYLE
            </h1>
            <p class="text-gray-600 text-base my-6">
              Browse through our diverse range of meticulously crafted garments,
              designed to bring out your individuality and cater to your sense
              of style.
            </p>
            <button
              class="bg-black text-white rounded-full font-semibold hover:bg-gray-700 transition duration-300 w-full h-12 md:w-52" onclick="document.getElementById('categoriesSection').scrollIntoView({ behavior: 'smooth' })"
            >
              Shop Now
            </button>

            <div class="mt-8 grid grid-cols-2 md:grid-cols-3 gap-4 text-center">
              <div class="flex flex-col items-start mx-auto">
                <span class="text-4xl font-bold">200+</span>
                <span class="text-sm text-gray-600">International Brands</span>
              </div>
              <div class="flex flex-col items-start mx-auto">
                <span class="text-4xl font-bold">2,000+</span>
                <span class="text-sm text-gray-600">High-Quality Products</span>
              </div>
              <div
                class="flex flex-col items-start col-span-2 md:col-auto mx-auto"
              >
                <span class="text-4xl font-bold">30,000+</span>
                <span class="text-sm text-gray-600">Happy Customers</span>
              </div>
            </div>
          </div>
          <div class="relative lg:w-1/2 w-full">
            <img
              src="${heroImgSrc}"
              class="h-auto object-cover"
              alt="Hero Image"
            />
            <img
              src="${starSSrc}"
              class="absolute top-[15%] left-0"
              alt="Star 1"
            />
            <img
              src="${starLSrc}"
              class="absolute top-[-5%] right-0"
              alt="Star 2"
            />
          </div>
        </div>
      </section>
  `;
}
