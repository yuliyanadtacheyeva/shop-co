import noUiSlider, { target as noUiSliderElement } from 'nouislider';
import 'nouislider/dist/nouislider.css';
import store from '../../redux/store.ts';
import { setPriceRange } from '../../redux/slices/filterSlice.ts';

let tempPriceRange = { min: 0, max: 0 };

export function initializeRangeSlider(
  sliderId: string,
  minPrice: number,
  maxPrice: number,
) {
  const sliderElement = document.getElementById(
    sliderId,
  ) as noUiSliderElement | null;
  if (!sliderElement) {
    console.error(`Slider element with ID ${sliderId} not found.`);
    return;
  }
  if (sliderElement.noUiSlider) {
    sliderElement.noUiSlider.updateOptions(
      { start: [minPrice, maxPrice], range: { min: minPrice, max: maxPrice } },
      false,
    );
  } else {
    noUiSlider.create(sliderElement, {
      start: [minPrice, maxPrice],
      connect: true,
      tooltips: true,
      range: { min: minPrice, max: maxPrice },
      format: {
        to: (value) => value.toFixed(),
        from: (value) => Number(value),
      },
    });
  }

  attachRangeSliderListener(sliderId);
}

function attachRangeSliderListener(sliderId: string) {
  const priceSlider = document.getElementById(
    sliderId,
  ) as noUiSliderElement | null;
  if (priceSlider?.noUiSlider) {
    priceSlider.noUiSlider.on('change', (values) => {
      tempPriceRange = {
        min: parseFloat(values[0].toString()),
        max: parseFloat(values[1].toString()),
      };
      store.dispatch(setPriceRange(tempPriceRange));
    });
  }
}

export function updateRangeSliderValues(
  sliderId: string,
  minPrice: number,
  maxPrice: number,
) {
  const sliderElement = document.getElementById(
    sliderId,
  ) as noUiSliderElement | null;

  if (sliderElement && sliderElement.noUiSlider) {
    sliderElement.noUiSlider.set([minPrice, maxPrice]);
  }
}
