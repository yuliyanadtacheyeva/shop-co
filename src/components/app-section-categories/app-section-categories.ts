import { AppCategoryCard } from '../app-card-category/app-card-category.ts';
import { AppLoadMoreButton } from '../app-button-loadmore/app-button-loadmore.ts';
import router from '../../router/router.ts';

let displayedCount = window.innerWidth > 768 ? 12 : 8;
const incrementCount =
  window.innerWidth > 768 && window.innerWidth < 1024 ? 3 : 4;
let categories: string[] = [];

function renderCategories(): string {
  const categoryCards = categories
    .slice(0, displayedCount)
    .map(AppCategoryCard)
    .join('');
  return `
    <div class="max-w-[1240px] mx-auto flex flex-col" id="categoriesContainer">
      <div class="px-4 mt-16 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
        ${categoryCards}
      </div>
      ${displayedCount < categories.length ? AppLoadMoreButton() : ''}
    </div>
  `;
}

export function AppCategoriesSection(fetchedCategories: string[]): string {
  categories = fetchedCategories;
  const sectionHTML = `<section id="categoriesSection">${renderCategories()}</section>`;

  setTimeout(() => attachCategoryEventListeners(), 0);

  return sectionHTML;
}

function attachCategoryEventListeners() {
  const categoriesSection = document.getElementById('categoriesSection');
  categoriesSection?.addEventListener('click', (event) => {
    const target = event.target as HTMLElement;
    if (target.classList.contains('category-card')) {
      const category = target.getAttribute('data-category');
      if (category) {
        router.navigate(`/category/${category}`);
      }
    } else if (target.id === 'loadMoreButton') {
      displayedCount += incrementCount;
      const categoriesContainer = document.getElementById(
        'categoriesContainer',
      );
      if (categoriesContainer) {
        categoriesContainer.innerHTML = renderCategories();
      }
    }
  });
}
