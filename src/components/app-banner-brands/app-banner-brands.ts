import versaceSrc from '../../assets/icons/brand-versace.svg';
import zaraSrc from '../../assets/icons/brand-zara.svg';
import gucciSrc from '../../assets/icons/brand-gucci.svg';
import pradaSrc from '../../assets/icons/brand-prada.svg';
import kleinSrc from '../../assets/icons/brand-klein.svg';

export function AppBannerBrands() {
  return `
        <section class="bg-black">
      <div class="mx-auto max-w-screen-xl px-4 py-7 sm:px-6 lg:px-8">
      <div class="flex flex-wrap justify-center lg:justify-between gap-8">
        <p class="flex justify-center items-center">
          <img src="${versaceSrc}" alt="Versace">
        </p>
        <p class="flex justify-center items-center">
          <img src="${zaraSrc}" alt="Zara">
        </p>
        <p href="#" class="flex justify-center items-center">
          <img src="${gucciSrc}" alt="Gucci">
        </p>
        <p href="#" class="flex justify-center items-center">
          <img src="${pradaSrc}" alt="Prada">
        </p>
        <p href="#" class="flex justify-center items-center">
          <img src="${kleinSrc}" alt="Calvin Klein">
        </p>
      </div>
    </div>
      </section>
  `;
}
