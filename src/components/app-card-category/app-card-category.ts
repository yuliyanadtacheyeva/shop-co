export function AppCategoryCard(category: string): string {
  const categoryName = category.replace('-', ' ');
  return `
    <div class="category-card bg-[#F0EEED] py-16 rounded-lg cursor-pointer text-left font-bold text-5xl md:text-6xl capitalize break-all" data-category="${category}">
      ${categoryName}
    </div>
  `;
}
