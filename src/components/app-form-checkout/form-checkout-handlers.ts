import store from '../../redux/store.ts';
import router from '../../router/router.ts';
import { validateInput } from '../../utils/validationUtils.ts';
import { fetchUserData } from '../../utils/apiUtils.ts';
import { deleteCart } from '../../redux/slices/cartSlice.ts';

async function populateFormData() {
  const state = store.getState();
  const user = state.user;

  if (user.isLoggedIn) {
    try {
      const userData = await fetchUserData(
        user.id,
        'firstName,lastName,maidenName,email,phone,address,city,postalCode',
      );
      const form = document.getElementById(
        'checkout-form',
      ) as HTMLFormElement | null;
      if (form) {
        form.firstName.value = userData.firstName || '';
        form.lastName.value = userData.lastName || '';
        form.maidenName.value = userData.maidenName || '';
        form.email.value = userData.email || '';
        form.phone.value = userData.phone || '';
        form.address.value = userData.address?.address || '';
        form.city.value = userData.address?.city || '';
        form.postalCode.value = userData.address?.postalCode || '';
      }
    } catch (error) {
      console.error('Error fetching or populating user data:', error);
    }
  }
}

async function handleCheckoutFormSubmit(event: Event) {
  event.preventDefault();
  const form = event.target as HTMLFormElement;
  let isFormValid = true;

  const inputs = form.querySelectorAll('input');
  inputs.forEach((input) => {
    if (!validateInput(input)) {
      isFormValid = false;
    }
  });

  if (!isFormValid) {
    console.error('Form validation failed');
    return;
  }

  store.dispatch(deleteCart());
  router.navigate('/payment');
}

export function initializeCheckoutForm() {
  populateFormData();

  const form = document.getElementById('checkout-form') as HTMLFormElement;
  if (form) {
    form.addEventListener('submit', handleCheckoutFormSubmit);
    form.querySelectorAll('input').forEach((input) => {
      input.addEventListener('input', () => validateInput(input));
    });
  }
}
