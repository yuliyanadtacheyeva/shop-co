export function AppFormCheckout() {
  return `
<form id="checkout-form" action="#" class="mx-auto border border-gray-300 rounded-3xl px-6 py-8" novalidate="">
  <fieldset>

    <div class="mb-5 relative">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="firstName"
             type="text"
             placeholder="First name"
             required
             autocomplete="given-name">
      <div id="firstName-validation" class="text-sm text-red-500 hidden">First name must be 3-32 characters long</div>
    </div>

    <div class="mb-5 relative">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="lastName"
             type="text"
             placeholder="Last name"
             required
             autocomplete="family-name">
      <div id="lastName-validation" class="text-sm text-red-500 hidden">Last name must be 3-32 characters long</div>
    </div>

    <div class="mb-5 relative">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="maidenName"
             type="text"
             placeholder="Maiden name"
             required
             autocomplete="additional-name">
      <div id="maidenName-validation" class="text-sm text-red-500 hidden">Maiden name must be 3-32 characters long</div>
    </div>

    <div class="mb-5 relative pt-5 border-t border-t-gray-200">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="email"
             type="email"
             placeholder="Email"
             required
             autocomplete="email">
      <div id="email-validation" class="text-sm text-red-500 hidden">Please enter a valid email</div>
    </div>

    <div class="mb-5 relative">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="phone"
             type="tel"
             placeholder="Phone +12 345 678 9012"
             required
             autocomplete="tel">
      <div id="phone-validation" class="text-sm text-red-500 hidden">Phone number must be in the format +12 345 678 9012</div>
    </div>

    <div class="mb-5 relative pt-5 border-t border-t-gray-200">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="address"
             type="text"
             placeholder="Address"
             required
             autocomplete="street-address">
      <div id="address-validation" class="text-sm text-red-500 hidden">Address cannot be empty</div>
    </div>

    <div class="mb-5 relative">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="city"
             type="text"
             placeholder="City"
             required
             autocomplete="address-level2">
      <div id="city-validation" class="text-sm text-red-500 hidden">City cannot be empty</div>
    </div>

    <div class="relative">
      <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
             name="postalCode"
             type="text"
             placeholder="Postal code"
             required
             autocomplete="postal-code">
      <div id="postalCode-validation" class="text-sm text-red-500 hidden">Postal code cannot be empty</div>
    </div>
  </fieldset>
</form>`;
}
