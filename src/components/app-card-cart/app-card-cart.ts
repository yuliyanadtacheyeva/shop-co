import { CartProduct } from '../../types/types.ts';
import { AppQuantitySelector } from '../app-quantity-selector/app-quantity-selector.ts';
import deleteIconSrc from '../../assets/icons/delete.svg';

export function createCartCard(cartProduct: CartProduct): string {
  const {
    id,
    title,
    price = 0,
    discountPercentage = 0,
    thumbnail,
    quantity,
  } = cartProduct;

  const discountedPrice = price - (price * discountPercentage) / 100;

  const discountLabel = discountPercentage
    ? `<span class="text-xs font-semibold text-red-600 bg-red-100 rounded-full px-2 py-1 ml-2">-${Math.round(
        discountPercentage,
      )}%</span>`
    : '';

  return `
    <div class="cart-card flex items-start p-6 border-b" id="cart-card-${id}">
      <div class="flex-shrink-0"><img class="h-24 w-24 rounded-lg object-cover" src="${thumbnail}" alt="${title}" /></div>
      <div class="flex-grow ml-4">
        <div class="flex justify-between items-center mb-5">
          <div class="text-lg font-bold text-gray-900">${title}</div>
          <img src="${deleteIconSrc}" alt="Delete" class="cursor-pointer" data-product-id="${id}">
        </div>
        <div class="flex justify-between items-center flex-wrap">
          <div class="flex items-center">
            <span class="text-xl font-bold text-gray-900">$${Math.round(
              discountedPrice,
            )}</span>
            ${
              discountPercentage > 0
                ? `<span class="text-sm line-through text-gray-500 ml-2">$${Math.round(
                    price,
                  )}</span>`
                : ''
            }
            ${discountLabel}
          </div>
          ${AppQuantitySelector(quantity, `cart-${id}`)}
        </div>
      </div>
    </div>
  `;
}
