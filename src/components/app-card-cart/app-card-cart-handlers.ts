import store, { AppDispatch } from '../../redux/store.ts';
import {
  updateCart,
  removeProductFromCart,
  deleteCart,
} from '../../redux/slices/cartSlice.ts';
import { CartProduct } from '../../types/types.ts';
import { fetchProductById } from '../../utils/apiUtils.ts';
import router from '../../router/router.ts';

export const initializeCartCardHandlers = async (cartProduct: CartProduct) => {
  const productId = cartProduct.id;
  const product = await fetchProductById(productId);
  const productStock = product?.stock ?? 0;

  const incrementButton = document.getElementById(
    `increment-button-cart-${productId}`,
  ) as HTMLButtonElement;
  const decrementButton = document.getElementById(
    `decrement-button-cart-${productId}`,
  ) as HTMLButtonElement;
  const deleteIcon = document.querySelector(
    `img[alt="Delete"][data-product-id="${productId}"]`,
  );
  const quantityInput = document.getElementById(
    `quantity-input-cart-${productId}`,
  ) as HTMLInputElement;

  if (!incrementButton || !decrementButton || !quantityInput || !deleteIcon) {
    console.error('Cart card elements not found');
    return;
  }
  quantityInput.setAttribute('disabled', '');

  const dispatch: AppDispatch = store.dispatch;

  incrementButton.addEventListener('click', () => {
    const cartProduct = store
      .getState()
      .cart.products.find((p) => p.id === productId);
    if (cartProduct && cartProduct.quantity < productStock) {
      dispatch(updateCart({ productId, quantity: 1 }));
      quantityInput.value = (cartProduct.quantity + 1).toString();
      incrementButton.disabled = cartProduct.quantity + 1 >= productStock;
      decrementButton.disabled = false;
    }
  });

  decrementButton.addEventListener('click', () => {
    const cartProduct = store
      .getState()
      .cart.products.find((p) => p.id === productId);
    if (cartProduct && cartProduct.quantity > 1) {
      dispatch(updateCart({ productId, quantity: -1 }));
      quantityInput.value = (cartProduct.quantity - 1).toString();
      decrementButton.disabled = cartProduct.quantity - 1 <= 0;
      incrementButton.disabled = false;
    } else if (cartProduct && cartProduct.quantity === 1) {
      removeProduct();
    }
  });

  const removeProduct = () => {
    dispatch(removeProductFromCart(productId));
    const cardElement = document.getElementById(`cart-card-${productId}`);
    if (cardElement) cardElement.remove();
    const updatedCartState = store.getState().cart;
    if (updatedCartState.products.length === 0) {
      dispatch(deleteCart());
      router.navigate('/');
    }
  };

  deleteIcon.addEventListener('click', removeProduct);
};
