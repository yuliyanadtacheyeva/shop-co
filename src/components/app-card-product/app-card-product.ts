import { AppStarRating } from '../app-star-rating/app-star-rating.ts';
import { Product } from '../../types/types.ts';

export function createProductCard(product: Product): string {
  const { id, title, price, discountPercentage, rating, thumbnail } = product;

  const discountedPrice = price - (price * discountPercentage) / 100;

  const ratingStars = AppStarRating(rating);

  const saleBadge = discountPercentage
    ? `<span class="absolute top-0 left-0 w-28 translate-y-4 -translate-x-6 -rotate-45 bg-black text-center text-sm text-white">Sale</span>`
    : '';

  const discountLabel = discountPercentage
    ? `<span class="rounded-full bg-red-200 px-3 py-1 text-sm text-red-500 font-medium">-${Math.round(
        discountPercentage,
      )}%</span>`
    : '';

  return `
<div
    class="product-card relative w-full max-w-xs mx-auto overflow-hidden rounded-lg bg-white shadow-md flex flex-col items-center justify-between cursor-pointer"
    data-productId="${id}"
  >
  <a href="#/product/${id}" class="mx-auto">
    <img
      class="h-60 w-auto mx-auto rounded-t-lg object-cover"
      src="${thumbnail}"
      alt="${title}"
    />
  </a>
  ${saleBadge}
  <div class="flex flex-col justify-between w-full h-full p-5">
    <div>
      <a href="#/product/${id}">
        <h5
          class="text-2xl font-semibold tracking-tight text-slate-900 capitalize"
        >
          ${title}
        </h5>
      </a>
      <div class="mt-2.5 flex items-center">
        ${ratingStars}
        <span class="ml-2 text-sm font-semibold text-slate-500"
          >${rating.toFixed(1)}/5</span
        >
      </div>
    </div>
    <div>
      <div class="flex items-center justify-between mt-5">
        <p>
          <span class="text-3xl font-bold text-slate-900"
            >$${Math.round(discountedPrice)}</span
          >
          ${
            discountPercentage > 0
              ? `<span
            class="text-sm text-slate-400 line-through"
            >$${Math.round(price)}</span
          >`
              : ''
          }
        </p>
        ${discountLabel}
      </div>
    </div>
  </div>
</div>
  `;
}
