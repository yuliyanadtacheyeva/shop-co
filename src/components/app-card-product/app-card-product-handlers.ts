import router from '../../router/router.ts';

export function attachProductClickListeners(containerSelector: string) {
  const productsContainer = document.querySelector(containerSelector);
  productsContainer?.addEventListener('click', (event) => {
    const target = event.target as HTMLElement;
    const card = target.closest('.product-card');
    if (card) {
      const productId = card.getAttribute('data-productId');
      if (productId) {
        router.navigate(`/product/${productId}`);
      }
    }
  });
}
