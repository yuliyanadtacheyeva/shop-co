export function AppFormPayment() {
  return `
  <form id="payment-form" action="#" class="mx-auto border border-gray-300 rounded-3xl px-6 py-8" novalidate>
    <fieldset>

      <div class="mb-5 relative">
        <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="cardNumber" type="text" placeholder="Card Number" required>
        <div id="cardNumber-validation" class="text-sm text-red-500 hidden">Card Number is invalid</div>
      </div>

      <div class="mb-5 relative">
        <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="cardExpire" type="text" placeholder="Card expire 00/00" required>
        <div id="cardExpire-validation" class="text-sm text-red-500 hidden">Card expire is invalid</div>
      </div>

      <div class="relative">
        <input class="appearance-none rounded-full w-full bg-gray-100 py-3 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="iban" type="text" placeholder="IBAN AB12 3456 7890 1234 5678" required>
        <div id="iban-validation" class="text-sm text-red-500 hidden">IBAN is invalid</div>
      </div>
    </fieldset>
  </form>
 `;
}
