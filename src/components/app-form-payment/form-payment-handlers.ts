import store from '../../redux/store.ts';
import router from '../../router/router.ts';
import { validateInput } from '../../utils/validationUtils.ts';
import { fetchUserData } from '../../utils/apiUtils.ts';

async function populateFormData() {
  const state = store.getState();
  const user = state.user;

  if (user.isLoggedIn) {
    try {
      const userData = await fetchUserData(user.id);
      const form = document.getElementById(
        'payment-form',
      ) as HTMLFormElement | null;
      if (form) {
        form.cardNumber.value = userData.bank.cardNumber || '';
        form.cardExpire.value = userData.bank.cardExpire || '';
        form.iban.value = userData.bank.iban || '';
      }
    } catch (error) {
      console.error('Error fetching or populating user data:', error);
    }
  }
}

function handlePaymentFormSubmit(event: Event) {
  event.preventDefault();
  const form = event.target as HTMLFormElement;
  let isFormValid = true;

  const inputs = form.querySelectorAll('input');
  inputs.forEach((input) => {
    if (!validateInput(input)) {
      isFormValid = false;
    }
  });

  if (!isFormValid) {
    console.error('Form validation failed');
    return;
  }

  router.navigate('/confirmation');
}

export function initializePaymentForm() {
  populateFormData();

  const form = document.getElementById('payment-form') as HTMLFormElement;
  if (form) {
    form.addEventListener('submit', handlePaymentFormSubmit);
    form.querySelectorAll('input').forEach((input) => {
      input.addEventListener('input', () => validateInput(input));
    });
  }
}
