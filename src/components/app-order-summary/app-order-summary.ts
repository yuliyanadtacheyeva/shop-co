export function AppOrderSummary(
  subtotal: number,
  discount: number,
  total: number,
  buttonText: string,
  buttonIdSuffix: string,
  isSubmitButton: boolean = true,
): string {
  const buttonType = isSubmitButton ? 'submit' : 'button';

  return `
    <div class="mb-[-100px] md:mb-0 bg-white shadow-lg rounded-lg p-6">
      <div class="font-bold text-xl mb-4">Order Summary</div>
      <div class="flex justify-between mb-2">
        <span>Subtotal</span>
        <span id="subtotal-value">$${subtotal}</span>
      </div>
      <div class="flex justify-between mb-2">
        <span>Discount</span>
        <span id="discount-value" class="text-red-500">-$${discount}</span>
      </div>
      <div class="flex justify-between border-t pt-4 mt-4 font-bold">
        <span>Total</span>
        <span id="total-value">$${total}</span>
      </div>
      <button id="order-button-${buttonIdSuffix}" type="submit"   form="${buttonIdSuffix}-form" type="${buttonType}" class="mt-6 bg-black text-white w-full py-3 rounded-full hover:scale-105 transition duration-300">${buttonText}</button>
    </div>
  `;
}
