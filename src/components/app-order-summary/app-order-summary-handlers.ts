import store from '../../redux/store.ts';
import { CartState } from '../../redux/slices/cartSlice.ts';

export function initializeOrderSummaryHandlers(
  buttonIdSuffix: string,
  action: (event: Event) => void,
) {
  const updateOrderSummaryValues = () => {
    const state: CartState = store.getState().cart;
    const subtotalElement = document.getElementById('subtotal-value');
    const discountElement = document.getElementById('discount-value');
    const totalElement = document.getElementById('total-value');

    if (subtotalElement) {
      subtotalElement.textContent = `$${state.total}`;
    }
    if (discountElement) {
      discountElement.textContent = `-$${state.total - state.discountedTotal}`;
    }
    if (totalElement) {
      totalElement.textContent = `$${state.discountedTotal}`;
    }
  };

  const orderSummaryButton = document.getElementById(
    `order-button-${buttonIdSuffix}`,
  );
  if (orderSummaryButton) {
    const eventType = buttonIdSuffix === 'cart' ? 'click' : 'submit';
    orderSummaryButton.addEventListener(eventType, action);
  }

  // Call immediately to set initial values
  updateOrderSummaryValues();

  store.subscribe(updateOrderSummaryValues);
}
