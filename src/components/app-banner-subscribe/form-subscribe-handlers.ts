import { validateInput } from '../../utils/validationUtils.ts';

async function handleSubscribeFormSubmit(event: Event) {
  event.preventDefault();
  const form = event.target as HTMLFormElement;
  let isFormValid = true;

  const inputs = form.querySelectorAll('input');
  inputs.forEach((input) => {
    if (!validateInput(input)) {
      isFormValid = false;
    }
  });

  if (!isFormValid) {
    console.error('Form validation failed');
    return;
  }

  await new Promise((resolve) => setTimeout(resolve, 1000));
  const subscribeBanner = document.querySelector('.subscribe-banner');
  if (subscribeBanner) {
    subscribeBanner.innerHTML =
      '<p class="w-full mx-auto text-center text-white text-3xl md:text-4xl font-poppins font-bold">Success! You\'ve subscribed to our newsletter.</p>';
  } else {
    console.error('Subscribe banner element not found');
  }
}

export function initializeSubscribeForm() {
  const form = document.getElementById('subscribe-form') as HTMLFormElement;
  if (form) {
    form.addEventListener('submit', handleSubscribeFormSubmit);
    form.querySelectorAll('input').forEach((input) => {
      input.addEventListener('input', () => validateInput(input));
    });
  }
}
