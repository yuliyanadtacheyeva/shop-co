import mailIconSrc from '../../assets/icons/mail.svg';
export function AppBannerSubscribe() {
  const host = document.createElement('section');
  host.classList.add('w-full', 'bg-white');

  host.innerHTML = `
    <div class="max-w-[1240px] mx-auto flex z-20">
    <div class="subscribe-banner w-full bg-black rounded-2xl mx-4 px-4 py-8 md:px-16 transform translate-y-1/2 flex items-center flex-col md:flex-row" >
    <div class="text-white text-center pr-8 md:text-left md:flex-1">
    <h3 class="text-3xl md:text-4xl font-poppins font-bold">STAY UP TO DATE ABOUT OUR LATEST OFFERS</h3>
    </div>

    <form id="subscribe-form" action="#" novalidate autocomplete="on" class="flex flex-col items-center md:flex-1" >
    <input type="email" name = "email" placeholder="Enter your email address" autocomplete="email" required class="appearance-none pl-8 pr-4 py-3 m-2 rounded-full w-full md:w-10/12 bg-no-repeat text-center focus:outline-none focus:shadow-outline" style="background-image: url('${mailIconSrc}'); background-position: 8% center;"/>

     <button type="submit" class="bg-white font-medium text-black py-3 m-2 rounded-full w-full md:w-10/12 hover:bg-gray-200 transition duration-300">
        Subscribe to Newsletter
     </button>
    </form>

    </div>
   </div>
   <div class="w-full bg-gray-100 min-h-32 flex z-40"></div>
  `;

  return host;
}
