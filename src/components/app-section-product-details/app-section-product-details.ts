import store from '../../redux/store.ts';
import { Product } from '../../types/types.ts';
import { AppStarRating } from '../app-star-rating/app-star-rating.ts';
import { AppQuantitySelector } from '../app-quantity-selector/app-quantity-selector.ts';

export function AppSectionProductDetails(product: Product): string {
  const { title, price, discountPercentage, rating, images, stock } = product;
  const discountedPrice = price - (price * discountPercentage) / 100;

  const ratingStars = AppStarRating(rating);
  const discountLabel = discountPercentage
    ? `<span class="rounded-full bg-red-200 px-3 py-1 text-sm text-red-500 font-medium">-${Math.round(
        discountPercentage,
      )}% off</span>`
    : '';

  const getRemainingStock = () => {
    const productInCart = store
      .getState()
      .cart.products.find((p) => p.id === product.id);
    const quantityInCart = productInCart ? productInCart.quantity : 0;
    return stock - quantityInCart;
  };

  const cartProduct = store
    .getState()
    .cart.products.find((p) => p.id === product.id);

  const quantityInCart = cartProduct ? cartProduct.quantity : 0;
  let initialStock = getRemainingStock();

  store.subscribe(() => {
    initialStock = getRemainingStock();
    const stockElement = document.querySelector('.stock-count');
    if (stockElement) {
      stockElement.textContent = `${initialStock} items`;
    }
  });

  return `
    <section class="product-details flex flex-col lg:flex-row lg:space-x-4">
    <div class="thumbnails-container hidden lg:flex lg:flex-col lg:space-y-4 lg:justify-between mr-4">
      ${images
        .slice(1, 4)
        .map(
          (img, index) => `
            <img
              src="${img}"
              alt="${title} thumbnail ${index + 1}"
              class="thumbnail cursor-pointer w-20 lg:w-auto lg:h-24 rounded-2xl object-cover"
              onclick="document.querySelector('.main-image').src='${img}'"
          />
        `,
        )
        .join('')}
  </div>
  <div class="w-full lg:w-1/2">
    <div class="flex justify-center items-center overflow-hidden h-[530px] bg-gray-100 rounded-3xl">
      <img
        src="${images[0]}"
        alt="${title}"
        class="main-image max-w-full max-h-full object-contain"
      />
    </div>
    <div class="thumbnails-container flex space-x-2 mt-4 justify-center lg:hidden">
      ${images
        .slice(1, 4)
        .map(
          (img, index) => `
            <img
              src="${img}"
              alt="${title} thumbnail ${index + 1}"
              class="thumbnail cursor-pointer w-20 rounded-2xl object-cover"
              onclick="document.querySelector('.main-image').src='${img}'"
            />
          `,
        )
        .join('')}
    </div>
  </div>

  <div class="product-details w-full lg:w-1/2 mt-6 lg:mt-0">
    <h2 class="product-title pb-3 font-poppins text-3xl font-bold">${title}</h2>
    <div class="rating pb-3">${ratingStars} <span>${rating}/5</span></div>
    <div class="pricing pb-3">
      <span class="discounted-price text-3xl font-bold">$${Math.round(
        discountedPrice,
      )}</span>
      ${
        discountPercentage > 0
          ? `<span class="original-price text-xl text-gray-400 font-bold line-through">$${Math.round(
              price,
            )}</span>`
          : ''
      }
      <span class="discount-label">${discountLabel}</span>
    </div>
    <p class="description text-sm text-gray-700 pb-4 border-b-2 border-b-gray-100">${
      product.description
    }</p>
    <div class="brand flex flex-col py-3">
      Brand:<span class="pt-3 font-poppins text-2xl font-bold pb-4 border-b-2 border-b-gray-100">${
        product.brand
      }</span>
    </div>
    <div class="stock flex flex-col py-3">
      In Stock: <span class="stock-count pt-3 font-poppins text-2xl font-bold pb-4 border-b-2 border-b-gray-100">${
        product.stock - quantityInCart
      } items</span>
    </div>

    <div class="flex justify-start">
      ${AppQuantitySelector(1, `product-${product.id}`)}
      <button class="add-to-cart-btn bg-black text-white font-bold w-full max-w-[360px] ml-4 py-3 px-4 rounded-full focus:outline-none focus:shadow-outline hover:scale-105 transition duration-300">Add to Cart</button>
    </div>
</section>
  `;
}
