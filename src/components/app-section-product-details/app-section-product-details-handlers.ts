import store, { AppDispatch } from '../../redux/store.ts';
import { Product } from '../../types/types.ts';
import { updateCart, addNewCart } from '../../redux/slices/cartSlice.ts';

export function initializeProductDetailsHandlers(product: Product) {
  const incrementButton = document.getElementById(
    `increment-button-product-${product.id}`,
  ) as HTMLButtonElement;
  const decrementButton = document.getElementById(
    `decrement-button-product-${product.id}`,
  ) as HTMLButtonElement;
  const quantityInput = document.getElementById(
    `quantity-input-product-${product.id}`,
  ) as HTMLInputElement;
  const addToCartBtn = document.querySelector(
    '.add-to-cart-btn',
  ) as HTMLButtonElement;

  function updateInputValue(newValue: number) {
    quantityInput.value = newValue.toString();
  }

  function updateQuantityInput() {
    const currentCartState = store.getState().cart;
    const currentProductInCart = currentCartState.products.find(
      (p) => p.id === product.id,
    );
    const currentQuantityInCart = currentProductInCart
      ? currentProductInCart.quantity
      : 0;
    const maxQuantity = product.stock - currentQuantityInCart;

    if (maxQuantity === 0) {
      quantityInput.value = '0';
      quantityInput.disabled = true;
      addToCartBtn.textContent = 'Out of Stock';
      addToCartBtn.disabled = true;
      incrementButton.disabled = true;
      decrementButton.disabled = true;
      addToCartBtn.classList.remove('hover:bg-gray-700');
      addToCartBtn.classList.add('bg-gray-400', 'cursor-default');
    } else {
      quantityInput.disabled = false;
      addToCartBtn.disabled = false;
      incrementButton.disabled = false;
      decrementButton.disabled = false;
      addToCartBtn.textContent = 'Add to Cart';
      addToCartBtn.classList.add('hover:bg-gray-700');
      addToCartBtn.classList.remove('bg-gray-400', 'cursor-default');

      if (parseInt(quantityInput.value) > maxQuantity) {
        updateInputValue(maxQuantity);
      }
    }
  }

  incrementButton?.addEventListener('click', () => {
    const inputValue = parseInt(quantityInput.value) || 1;
    if (inputValue < product.stock) {
      updateInputValue(inputValue + 1);
    }
  });

  decrementButton?.addEventListener('click', () => {
    const inputValue = parseInt(quantityInput.value) || 0;
    if (inputValue > 1) {
      updateInputValue(inputValue - 1);
    }
  });

  quantityInput.addEventListener('change', updateQuantityInput);
  store.subscribe(updateQuantityInput);
  updateQuantityInput(); // Initial call to set up correct state

  addToCartBtn?.addEventListener('click', () => handleAddToCart(product));
}

function handleAddToCart(product: Product) {
  const quantityInput = document.getElementById(
    `quantity-input-product-${product.id}`,
  ) as HTMLInputElement;
  const quantityToAdd = parseInt(quantityInput.value, 10) || 0;

  if (quantityToAdd > 0) {
    const dispatch: AppDispatch = store.dispatch;
    const cartId = store.getState().cart.id;

    if (cartId) {
      dispatch(updateCart({ productId: product.id, quantity: quantityToAdd }));
    } else {
      dispatch(addNewCart({ product, quantity: quantityToAdd }));
    }
  } else {
    console.error('Invalid quantity: Cannot add items that are out of stock.');
  }
}
