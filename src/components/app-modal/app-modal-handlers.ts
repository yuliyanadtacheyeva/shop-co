export function initializeModal() {
  const filterIcon = document.querySelector('.filter-icon');
  const filterModal = document.getElementById('filter-modal');
  const closeModalButton = document.getElementById('close-modal');
  const resetFilterModalButton = document.querySelector(
    '#filter-modal .reset-filter',
  );

  function openModal() {
    filterModal?.classList.remove('hidden');
    filterModal?.classList.add('flex');
    document.body.style.overflow = 'hidden';

    window.addEventListener('resize', closeModalOnResize);
  }

  filterIcon?.addEventListener('click', openModal);

  function closeModalOnResize() {
    if (window.innerWidth >= 1024) {
      closeModal();
    }
  }

  function closeModal() {
    filterModal?.classList.add('hidden');
    filterModal?.classList.remove('flex');
    document.body.style.overflow = '';
    window.removeEventListener('resize', closeModalOnResize);
  }

  filterIcon?.addEventListener('click', openModal);
  closeModalButton?.addEventListener('click', closeModal);
  resetFilterModalButton?.addEventListener('click', closeModal);
  window.addEventListener('click', (event) => {
    if (event.target === filterModal) {
      closeModal();
    }
  });
}
