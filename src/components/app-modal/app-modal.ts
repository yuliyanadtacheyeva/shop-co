import { AppPanelFilters } from '../app-panel-filters/app-panel-filters.ts';

export function AppFiltersModal(brands: string[], sliderId: string) {
  return `
  <div id="filter-modal" class="fixed inset-0 bg-black bg-opacity-50 hidden justify-center items-end z-50">
    <div class="modal-content-container relative bg-white rounded-t-lg p-8 w-full max-w-md">
      <button id="close-modal" class="absolute top-0 right-[40%] mt-4 mr-4 w-8 h-8 flex items-center justify-center rounded-full text-black">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
        </svg>
      </button>
      <div id="filters-panel-modal">
      ${AppPanelFilters(brands, sliderId)}
      </div>
    </div>
  </div>
`;
}
