import { AppBannerSignup } from './app-banner-signup.ts';
import router from '../../router/router.ts';
import store from '../../redux/store.ts';

export function initializeBannerSignup() {
  const banner = AppBannerSignup();

  const signUpLink = banner.querySelector('#signUpLink');
  signUpLink?.addEventListener('click', (event) => {
    event.preventDefault();
    router.navigate('/register');
  });

  const closeBanner = banner.querySelector('#closeBanner');
  closeBanner?.addEventListener('click', () => {
    banner.style.display = 'none';
  });

  return banner;
}

export function updateBannerState() {
  const state = store.getState();
  const existingBanner = document.getElementById('bannerSignup');
  if (!state.user.isLoggedIn && !existingBanner) {
    const newBanner = initializeBannerSignup();
    document.querySelector('#app')?.prepend(newBanner);
  } else if (state.user.isLoggedIn && existingBanner) {
    existingBanner.remove();
  }
}
