export function AppBannerSignup(): HTMLElement {
  const banner = document.createElement('section');
  banner.id = 'bannerSignup';
  banner.classList.add('bg-black', 'px-6');

  banner.innerHTML = `
    <div class="mx-auto flex justify-center max-w-screen-xl px-4 sm:px-6 lg:px-8 py-2 relative text-white text-lg">
      <p class="font-normal">Sign up and get 20% off your first order. <span class="underline cursor-pointer font-medium" id="signUpLink">Sign Up Now</span></p>
      <button class="absolute right-0 top-1 text-white text-2xl" id="closeBanner">&times;</button>
    </div>
    `;

  return banner;
}
