export function AppStarRating(rating: number) {
  let starsHtml = '';
  for (let i = 1; i <= 5; i++) {
    if (rating >= i) {
      starsHtml += '<i class="fas fa-star text-yellow-500"></i> ';
    } else if (rating >= i - 0.5) {
      starsHtml += '<i class="fas fa-star-half-alt text-yellow-500"></i> ';
    } else {
      starsHtml += '<i class="far fa-star text-yellow-500"></i> ';
    }
  }
  return starsHtml;
}
