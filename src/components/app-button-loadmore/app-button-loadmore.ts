export function AppLoadMoreButton(): string {
  return `
    <div
    class="mt-20 flex items-center before:flex-1  before:bg-[#F0EEED] before:h-1.5 before:content-[''] after:h-1.5 after:flex-1 after:bg-[#F0EEED]  after:content-['']">
  <button id="loadMoreButton" type="button"
  class="load-more-button flex items-center rounded-full border-4 border-gray-200 bg-secondary-50 px-3 py-2 text-center text-3xl font-medium text-gray-900 hover:bg-gray-100">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="mr-1 h-4 w-4">
  <path fill-rule="evenodd"
  d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
  clip-rule="evenodd" />
    </svg>
  View More
  </button>
  </div>
  `;
}
