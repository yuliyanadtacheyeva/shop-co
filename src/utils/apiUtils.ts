import * as types from '../types/types.ts';

// Function to log and handle API errors
function handleApiError(error: unknown) {
  if (error instanceof Error) {
    console.error('API error:', error.message);
  } else {
    console.error('Unknown error:', error);
  }
}

export async function authenticateUser(username: string, password: string) {
  try {
    const response = await fetch('https://dummyjson.com/auth/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password }),
    });

    if (!response.ok) {
      throw new Error(`Login failed: ${response.status}`);
    }

    return await response.json();
  } catch (error) {
    handleApiError(error);
    throw error;
  }
}

export async function fetchUserData(
  userId: number | null,
  selectQuery?: string,
) {
  try {
    const queryParam = selectQuery ? `?select=${selectQuery}` : '';
    const response = await fetch(
      `https://dummyjson.com/users/${userId}${queryParam}`,
    );
    if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
    return await response.json();
  } catch (error) {
    handleApiError(error);
    return null;
  }
}

export async function fetchCategories(): Promise<string[]> {
  try {
    const response = await fetch('https://dummyjson.com/products/categories');
    if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
    return await response.json();
  } catch (error) {
    handleApiError(error);
    return [];
  }
}

export async function fetchProductsByCategory(
  category: string,
  limit?: number,
  skip?: number,
): Promise<types.Product[]> {
  let url = `https://dummyjson.com/products/category/${category}`;

  // Construct query string with limit and skip if they are provided
  const queryParams = new URLSearchParams();
  if (limit !== undefined) {
    queryParams.append('limit', limit.toString());
  }
  if (skip !== undefined) {
    queryParams.append('skip', skip.toString());
  }

  if (queryParams.toString()) {
    url += `?${queryParams}`;
  }

  const response = await fetch(url);
  const data = await response.json();
  return data.products as types.Product[];
}

export async function fetchProductById(
  productId: number,
): Promise<types.Product | null> {
  try {
    const response = await fetch(`https://dummyjson.com/products/${productId}`);
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }
    return (await response.json()) as types.Product;
  } catch (error) {
    handleApiError(error);
    return null;
  }
}

export const addCart = async (
  userId: number,
  products: types.CartProductUpdate[],
): Promise<types.CartResponse> => {
  const response = await fetch('https://dummyjson.com/carts/add', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ userId, products }),
  });
  return (await response.json()) as types.CartResponse;
};

// DUMMYJSON RETURNS CARTID NOT FOUND FOR ADDED CARTS, SO UPDATE/DELETE CART1 AND NOT CARTID
export const updateCart = async (
  products: types.CartProductUpdate[],
): Promise<types.CartResponse> => {
  const response = await fetch(`https://dummyjson.com/carts/1`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ products }),
  });
  return (await response.json()) as types.CartResponse;
};

export const deleteCart = async (): Promise<types.CartResponse> => {
  const response = await fetch(`https://dummyjson.com/carts/1`, {
    method: 'DELETE',
  });
  return (await response.json()) as types.CartResponse;
};
