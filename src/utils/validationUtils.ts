export function validateEmail(email: string): boolean {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export function validateUsername(username: string): boolean {
  return username.length >= 3 && username.length <= 32;
}

export function validatePassword(password: string): boolean {
  return password.length >= 3 && password.length <= 32;
}

export function validateName(name: string): boolean {
  return name.length >= 3 && name.length <= 32;
}

export function validatePhone(phone: string): boolean {
  const re = /^\+\d{1,3}( \d{3,4}){3}$/;
  return re.test(phone);
}

export function validateAddress(address: string): boolean {
  return address.trim().length > 0;
}

export function validateCity(city: string): boolean {
  return city.trim().length > 0;
}

export function validatePostalCode(postalCode: string): boolean {
  return postalCode.trim().length > 0;
}

export function validateCardNumber(cardNumber: string): boolean {
  const sanitized = cardNumber.replace(/\s+/g, '');
  return sanitized.length >= 12 && sanitized.length <= 19; // Typical length of card numbers
}

export function validateCardExpire(cardExpire: string): boolean {
  const re = /^(0[1-9]|1[0-2])\/[0-9]{2}$/;
  return re.test(cardExpire);
}

export function validateIBAN(iban: string): boolean {
  const re = /^[A-Z]{2}[0-9A-Z\s]{14,31}$/;
  return re.test(iban);
}

export function validateInput(inputElement: HTMLInputElement): boolean {
  let isValid = false;

  switch (inputElement.name) {
    case 'email':
      isValid = validateEmail(inputElement.value);
      break;
    case 'username':
      isValid = validateUsername(inputElement.value);
      break;
    case 'password':
      isValid = validatePassword(inputElement.value);
      break;
    case 'firstName':
    case 'lastName':
    case 'maidenName':
      isValid = validateName(inputElement.value);
      break;
    case 'phone':
      isValid = validatePhone(inputElement.value);
      break;
    case 'address':
      isValid = validateAddress(inputElement.value);
      break;
    case 'city':
      isValid = validateCity(inputElement.value);
      break;
    case 'postalCode':
      isValid = validatePostalCode(inputElement.value);
      break;
    case 'cardNumber':
      isValid = validateCardNumber(inputElement.value);
      break;
    case 'cardExpire':
      isValid = validateCardExpire(inputElement.value);
      break;
    case 'iban':
      isValid = validateIBAN(inputElement.value);
      break;
  }

  const validationElement = document.getElementById(
    inputElement.name + '-validation',
  ) as HTMLDivElement;
  if (!isValid) {
    inputElement.classList.add('ring-red-500', 'ring-2');
    validationElement?.classList.remove('hidden');
  } else {
    inputElement.classList.remove('ring-red-500', 'ring-2');
    validationElement?.classList.add('hidden');
  }

  return isValid;
}
