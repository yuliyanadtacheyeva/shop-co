import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface UserState {
  id: number | null;
  isLoggedIn: boolean;
  token: string | null;
}

const initialState: UserState = {
  id: null,
  isLoggedIn: false,
  token: null,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserLogin: (
      state,
      action: PayloadAction<{ id: number; token: string; isLoggedIn: boolean }>,
    ) => {
      state.id = action.payload.id;
      state.isLoggedIn = action.payload.isLoggedIn;
      state.token = action.payload.token;
    },
    clearUserData: (state) => {
      state.id = null;
      state.isLoggedIn = false;
      state.token = null;
    },
  },
});

export const { setUserLogin, clearUserData } = userSlice.actions;
export default userSlice.reducer;
