import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store.ts';
import * as api from '../../utils/apiUtils.ts';
import { CartProduct, CartResponse, Product } from '../../types/types.ts';

export interface CartState {
  id: number | null;
  products: CartProduct[];
  total: number;
  discountedTotal: number;
  userId: number | null;
  isDeleted: boolean;
}

const initialState: CartState = {
  id: null,
  products: [],
  total: 0,
  discountedTotal: 0,
  userId: null,
  isDeleted: false,
};

export const addNewCart = createAsyncThunk<
  CartResponse,
  { product: Product; quantity: number },
  { state: RootState }
>('cart/addNewCart', async ({ product, quantity }, { getState }) => {
  const state: RootState = getState();
  const userId = state.user.id || 1;
  return await api.addCart(userId, [{ id: product.id, quantity }]);
});

export const updateCart = createAsyncThunk<
  CartResponse,
  { productId: number; quantity: number },
  { state: RootState }
>('cart/updateCart', async ({ productId, quantity }, { getState }) => {
  const state = getState();

  const existingProductIndex = state.cart.products.findIndex(
    (p) => p.id === productId,
  );

  const updatedProducts = [...state.cart.products];

  if (existingProductIndex !== -1) {
    updatedProducts[existingProductIndex] = {
      ...updatedProducts[existingProductIndex],
      quantity: updatedProducts[existingProductIndex].quantity + quantity,
    };
  } else {
    const productToAdd = await api.fetchProductById(productId);
    if (productToAdd) {
      updatedProducts.push({ ...productToAdd, quantity });
    }
  }

  // Prepare the payload for the updateCart API
  const cartProductsUpdate = updatedProducts.map((product) => ({
    id: product.id,
    quantity: product.quantity,
  }));

  return await api.updateCart(cartProductsUpdate);
});

export const deleteCart = createAsyncThunk<void, void, { state: RootState }>(
  'cart/deleteCart',
  async () => {
    await api.deleteCart();
  },
);

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    removeProductFromCart: (state, action: PayloadAction<number>) => {
      const index = state.products.findIndex((p) => p.id === action.payload);
      if (index >= 0) {
        state.products.splice(index, 1);

        state.total = state.products.reduce(
          (total, product) =>
            total + (Math.round(<number>product.price) ?? 0) * product.quantity,
          0,
        );
        state.discountedTotal = state.products.reduce((total, product) => {
          const price = Math.round(<number>product.price) ?? 0;
          const discountPercentage = product.discountPercentage ?? 0;
          const discountedPrice = Math.round(
            price - (price * discountPercentage) / 100,
          );
          return total + discountedPrice * product.quantity;
        }, 0);
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(addNewCart.fulfilled, (state, action) => {
        state.id = action.payload.id;
        state.products = action.payload.products;
        state.total = action.payload.total;
        state.discountedTotal = action.payload.discountedTotal;
        state.userId = action.payload.userId;
        state.isDeleted = action.payload.isDeleted || false;
      })
      .addCase(updateCart.fulfilled, (state, action) => {
        state.products = action.payload.products;
        state.total = action.payload.total;
        state.discountedTotal = action.payload.discountedTotal;
      })
      .addCase(deleteCart.fulfilled, () => {
        return initialState;
      });
  },
});

export const { removeProductFromCart } = cartSlice.actions;

export default cartSlice.reducer;
