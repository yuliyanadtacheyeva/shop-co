import { createSlice } from '@reduxjs/toolkit';
import { Product } from '../../types/types.ts';

interface CartState {
  allProducts: Product[];
  filteredProducts: Product[];
  allBrands: string[];
  selectedBrands: string[];
  priceRange: { min: number; max: number };
}

const initialState: CartState = {
  allProducts: [],
  filteredProducts: [],
  allBrands: [],
  selectedBrands: [],
  priceRange: { min: 0, max: 0 },
};

const calculateDiscountedPrice = (product: Product) => {
  return Math.round(
    product.price - (product.price * product.discountPercentage) / 100,
  );
};

const filterProducts = (
  products: Product[],
  selectedBrands: string[],
  priceRange: { min: number; max: number },
): Product[] => {
  return products.filter((product) => {
    const discountedPrice = calculateDiscountedPrice(product);
    const isBrandSelected =
      selectedBrands.length === 0 || selectedBrands.includes(product.brand);
    const isWithinPriceRange =
      discountedPrice >= priceRange.min && discountedPrice <= priceRange.max;
    return isBrandSelected && isWithinPriceRange;
  });
};

const filterSlice = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    setInitialProducts: (state, action) => {
      state.allProducts = action.payload;
      state.filteredProducts = action.payload;

      const discountedPrices = action.payload.map(calculateDiscountedPrice);
      state.priceRange = {
        min: Math.min(...discountedPrices),
        max: Math.max(...discountedPrices),
      };

      state.allBrands = Array.from(
        new Set(action.payload.map((p: Product) => p.brand as string)),
      );
    },
    toggleBrand: (state, action) => {
      const brandIndex = state.selectedBrands.indexOf(action.payload);
      if (brandIndex >= 0) state.selectedBrands.splice(brandIndex, 1);
      else state.selectedBrands.push(action.payload);
    },
    setPriceRange: (state, action) => {
      state.priceRange = action.payload;
    },
    applyFilters: (state) => {
      state.filteredProducts = filterProducts(
        state.allProducts,
        state.selectedBrands,
        state.priceRange,
      );
    },
    resetFilters: (state) => {
      state.selectedBrands = [];
      const discountedPrices = state.allProducts.map(calculateDiscountedPrice);
      state.priceRange = {
        min: Math.min(...discountedPrices),
        max: Math.max(...discountedPrices),
      };
      state.filteredProducts = state.allProducts;
    },
  },
});

export const {
  setInitialProducts,
  toggleBrand,
  setPriceRange,
  applyFilters,
  resetFilters,
} = filterSlice.actions;

export default filterSlice.reducer;
