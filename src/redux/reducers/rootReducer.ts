import { combineReducers } from '@reduxjs/toolkit';
import userReducer from '../slices/userSlice';
import cartReducer from '../slices/cartSlice.ts';
import filterReducer from '../slices/filterSlice.ts';

const rootReducer = combineReducers({
  user: userReducer,
  cart: cartReducer,
  filter: filterReducer,
});

export default rootReducer;
