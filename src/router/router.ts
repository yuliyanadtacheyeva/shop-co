import Navigo from 'navigo';
import HomePage from '../pages/HomePage/HomePage.ts';
import LoginPage from '../pages/LoginPage/LoginPage.ts';
import RegisterPage from '../pages/RegisterPage/RegisterPage.ts';
import CategoryPage from '../pages/CategoryPage/CategoryPage.ts';
import CheckoutPage from '../pages/CheckoutPage/CheckoutPage.ts';
import PaymentPage from '../pages/PaymentPage/PaymentPage.ts';
import ProductPage from '../pages/ProductPage/ProductPage.ts';
import CartPage from '../pages/CartPage/CartPage.ts';
import ConfirmationPage from '../pages/ConfirmationPage/ConfirmationPage.ts';

export type RouteParams = {
  data: {
    [key: string]: string;
  };
  params: {
    [key: string]: string | null;
  };
};

type RouteHandler = (params: RouteParams) => Promise<string>;

const router = new Navigo('/', { hash: true });

const handleAsyncRoute = async (
  routeHandler: RouteHandler,
  params: RouteParams = { data: {}, params: {} },
) => {
  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = await routeHandler(params);
    window.scrollTo(0, 0);
  }
};

router
  .on({
    '/': () => handleAsyncRoute(HomePage),
    '/login': () => handleAsyncRoute(LoginPage),
    '/register': () => handleAsyncRoute(RegisterPage),
    '/checkout': () => handleAsyncRoute(CheckoutPage),
    '/payment': () => handleAsyncRoute(PaymentPage),
    '/product/:productId': (params: RouteParams) =>
      handleAsyncRoute(ProductPage, params),
    '/category/:categoryName': (params: RouteParams) =>
      handleAsyncRoute(CategoryPage, params),
    '/cart': () => handleAsyncRoute(CartPage),
    '/cart/:cartId': (params: RouteParams) =>
      handleAsyncRoute(CartPage, params),
    '/confirmation': () => handleAsyncRoute(ConfirmationPage),
  })
  .resolve();

router.notFound(() => {
  router.navigate('/');
});

export default router;
