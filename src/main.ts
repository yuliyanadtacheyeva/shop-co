import './style.css';
import router from './router/router.ts';
import store from './redux/store.ts';
import { updateBannerState } from './components/app-banner-signup/banner-signup-handlers.ts';
import { initializeHeaderHandlers } from './components/app-header/header-handlers.ts';
import { AppHeader } from './components/app-header/app-header.ts';
import { AppBannerSubscribe } from './components/app-banner-subscribe/app-banner-subscribe.ts';
import { initializeSubscribeForm } from './components/app-banner-subscribe/form-subscribe-handlers.ts';
import { AppFooter } from './components/app-footer/app-footer.ts';

document.addEventListener('DOMContentLoaded', () => {
  router.resolve();

  document.querySelector('#app')?.prepend(AppHeader());
  document.querySelector('#app')?.append(AppBannerSubscribe());
  document.querySelector('#app')?.append(AppFooter());

  updateBannerState();
  store.subscribe(updateBannerState);

  initializeHeaderHandlers();
  initializeSubscribeForm();
});
