import { RouteParams } from '../../router/router.ts';
import {
  AppBreadcrumbs,
  setupBreadcrumbsListener,
} from '../../components/app-breadcrumbs/app-breadcrumbs.ts';
import { fetchProductById } from '../../utils/apiUtils.ts';
import { AppSectionProductDetails } from '../../components/app-section-product-details/app-section-product-details.ts';
import { initializeProductDetailsHandlers } from '../../components/app-section-product-details/app-section-product-details-handlers.ts';
import { AppSectionRelatedProducts } from '../../components/app-section-related-products/app-section-related-products.ts';
import { initializeRelatedProductsHandlers } from '../../components/app-section-related-products/app-section-related-products-handlers.ts';

const ProductPage = async (params: RouteParams): Promise<string> => {
  const productIdNumber = +params.data.productId;
  const product = await fetchProductById(productIdNumber);

  if (!product) {
    return '<p>Product not found</p>';
  }

  const productDetailsHtml = AppSectionProductDetails(product);

  const relatedProductsHtml = await AppSectionRelatedProducts(
    product.category,
    5,
    productIdNumber,
  );

  const breadcrumbs = AppBreadcrumbs(`${product.category}`, `${product.title}`);

  const pageContent = `
    <div class="product-page-container mx-auto max-w-screen-xl p-4 md:p-8">
      <div id="breadcrumbs">${breadcrumbs}</div>
        ${productDetailsHtml}
        ${relatedProductsHtml}
    </div>
  `;

  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = pageContent;
    setTimeout(() => {
      setupBreadcrumbsListener();
      initializeProductDetailsHandlers(product);
      initializeRelatedProductsHandlers();
    }, 0);
  }

  return pageContent;
};

export default ProductPage;
