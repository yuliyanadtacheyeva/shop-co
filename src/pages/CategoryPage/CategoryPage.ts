import { RouteParams } from '../../router/router.ts';
import { fetchProductsByCategory } from '../../utils/apiUtils.ts';
import store from '../../redux/store.ts';
import { setInitialProducts } from '../../redux/slices/filterSlice.ts';
import {
  AppBreadcrumbs,
  setupBreadcrumbsListener,
} from '../../components/app-breadcrumbs/app-breadcrumbs.ts';
import { AppPanelFilters } from '../../components/app-panel-filters/app-panel-filters.ts';
import { AppFiltersModal } from '../../components/app-modal/app-modal.ts';
import { initializeModal } from '../../components/app-modal/app-modal-handlers.ts';
import { createProductCard } from '../../components/app-card-product/app-card-product.ts';
import { attachProductClickListeners } from '../../components/app-card-product/app-card-product-handlers.ts';
import filterIconSrc from '../../assets/icons/filter.svg';
import { initializeFilterPanel } from '../../components/app-panel-filters/app-panel-filters-handlers.ts';

const CategoryPage = async (params: RouteParams): Promise<string> => {
  const categoryName = params.data.categoryName;
  if (!categoryName) return '<p>Category not found</p>';

  await loadAndSetInitialProducts(categoryName);
  const pageContent = buildPageContent(categoryName);
  renderPageContent(pageContent);
  return pageContent;
};

async function loadAndSetInitialProducts(categoryName: string) {
  const allProducts = await fetchProductsByCategory(categoryName);
  store.dispatch(setInitialProducts(allProducts));
}

function buildPageContent(categoryName: string) {
  const breadcrumbs = AppBreadcrumbs(categoryName);
  const productsHTML = getProductsHTML();
  const filterPanelAsideHTML = AppPanelFilters(
    store.getState().filter.allBrands,
    'price-slider-aside',
  );
  const filterPanelModalHTML = AppFiltersModal(
    store.getState().filter.allBrands,
    'price-slider-modal',
  );

  return `
    <div class="mx-auto max-w-screen-xl p-4">
      <div id="breadcrumbs">${breadcrumbs}</div>
      <div class="main-content-container flex flex-col lg:flex-row justify-between">
        <aside class="hidden lg:block w-1/5">
          <div id='filters-panel-aside' class="filters-container border border-gray-300 rounded-3xl p-6">${filterPanelAsideHTML}</div>
        </aside>
        <section class="w-full lg:w-3/4 pb-12 border-b-2 border-b-gray-100">
          <div class="flex justify-between">
            <h1 class="category-title capitalize font-bold text-4xl mb-5">${categoryName}</h1>
            <div class="filter-icon w-10 h-10 border border-gray-300 rounded-full flex items-center justify-center hover:scale-125 transition duration-300 cursor-pointer lg:hidden">
              <img src="${filterIconSrc}" alt="filter icon" class="w-6 h-8">
            </div>
          </div>
          <div class="products-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">${productsHTML}</div>
        </section>
      </div>
      ${filterPanelModalHTML}
    </div>
  `;
}

function renderPageContent(content: string) {
  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = content;
    setTimeout(() => {
      setupEventListeners();
    }, 0);
  }
}

function setupEventListeners() {
  setupBreadcrumbsListener();
  attachProductClickListeners('.products-container');
  initializeFilterPanel(
    '#filters-panel-aside',
    'price-slider-aside',
    store.getState().filter.priceRange.min,
    store.getState().filter.priceRange.max,
  );
  initializeModal();
  initializeFilterPanel(
    '#filters-panel-modal',
    'price-slider-modal',
    store.getState().filter.priceRange.min,
    store.getState().filter.priceRange.max,
  );
  setupReduxSubscription();
}

function getProductsHTML() {
  return store
    .getState()
    .filter.filteredProducts.map(createProductCard)
    .join('');
}

function setupReduxSubscription() {
  let previousFilteredProducts = store.getState().filter.filteredProducts;
  store.subscribe(() => {
    const currentFilteredProducts = store.getState().filter.filteredProducts;
    if (previousFilteredProducts !== currentFilteredProducts) {
      const productsHTML = currentFilteredProducts
        .map(createProductCard)
        .join('');
      const productsContainer = document.querySelector('.products-container');
      if (productsContainer) {
        productsContainer.innerHTML = productsHTML;
      }
      previousFilteredProducts = currentFilteredProducts;
    }
  });
}

export default CategoryPage;
