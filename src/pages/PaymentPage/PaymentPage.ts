import store from '../../redux/store.ts';
import router from '../../router/router.ts';
import {
  AppBreadcrumbs,
  setupBreadcrumbsListener,
} from '../../components/app-breadcrumbs/app-breadcrumbs.ts';
import { AppFormPayment } from '../../components/app-form-payment/app-form-payment.ts';
import { initializePaymentForm } from '../../components/app-form-payment/form-payment-handlers.ts';
import { AppOrderSummary } from '../../components/app-order-summary/app-order-summary.ts';
import { initializeOrderSummaryHandlers } from '../../components/app-order-summary/app-order-summary-handlers.ts';

const PaymentPage = async () => {
  const state = store.getState().cart;
  const breadcrumbs = AppBreadcrumbs('Payment');
  const appFormPayment = AppFormPayment();
  const orderSummaryHtml = AppOrderSummary(
    state.total,
    state.total - state.discountedTotal,
    state.discountedTotal,
    'Place an Order',
    'payment',
  );

  const pageContent = `
    <div class="product-page-container mx-auto max-w-screen-xl p-4 md:p-8">
      <div id="breadcrumbs">${breadcrumbs}</div>
      <h2 class="text-start font-poppins font-bold text-4xl pb-12">Checkout</h2>
        <div class="flex flex-col md:flex-row items-start">
    <div class="md:w-2/3 w-full m-2">${appFormPayment}</div>
    <div class="md:w-1/3 w-full m-2">${orderSummaryHtml}</div>
  </div>
  </div>
  `;

  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = pageContent;
    setTimeout(() => {
      setupBreadcrumbsListener();
      initializePaymentForm();
      initializeOrderSummaryHandlers('checkout', (event: Event) => {
        event.preventDefault();
        router.navigate('/confirmation');
      });
    }, 0);
  }

  return pageContent;
};

export default PaymentPage;
