import { AppHero } from '../../components/app-hero/app-hero.ts';
import { AppBannerBrands } from '../../components/app-banner-brands/app-banner-brands.ts';
import { AppCategoriesSection } from '../../components/app-section-categories/app-section-categories.ts';
import { fetchCategories } from '../../utils/apiUtils.ts';

const HomePage = async () => {
  const categories = await fetchCategories();
  return `
        ${AppHero()}
        ${AppBannerBrands()}
        ${AppCategoriesSection(categories)}
    `;
};

export default HomePage;
