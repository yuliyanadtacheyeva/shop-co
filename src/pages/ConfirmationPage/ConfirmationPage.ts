import router from '../../router/router.ts';
import {
  AppBreadcrumbs,
  setupBreadcrumbsListener,
} from '../../components/app-breadcrumbs/app-breadcrumbs.ts';

const ConfirmationPage = async () => {
  const breadcrumbs = AppBreadcrumbs('Checkout');

  const pageContent = `
    <div class="product-page-container mx-auto max-w-screen-xl p-4 md:p-8">
      <div id="breadcrumbs">${breadcrumbs}</div>
        <h2 class="text-start font-poppins font-bold text-4xl pb-12">Order Confirmation</h2>
        <div class="text-xl border rounded-2xl max-w-4xl inline-block font-bold border-gray-300 p-4">Success! Your order has been confirmed. Please check out your email address to track delivery progress.
        <div class="font-medium mt-4">You will be redirected to Home Page now!</div>
        </div>
    </div>
  `;

  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = pageContent;
    setTimeout(() => {
      setupBreadcrumbsListener();
    }, 0);
    setTimeout(() => {
      router.navigate('/');
    }, 5000);
  }

  return pageContent;
};

export default ConfirmationPage;
