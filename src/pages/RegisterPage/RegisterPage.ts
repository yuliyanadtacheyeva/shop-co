import { AppFormAuth } from '../../components/app-form-auth/app-form-auth.ts';
import { initializeAuthForm } from '../../components/app-form-auth/form-auth-handlers.ts';

const RegisterPage = async () => {
  const pageContent = AppFormAuth('register');
  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = pageContent;

    setTimeout(() => {
      initializeAuthForm();
    }, 0);
  }
  return pageContent;
};

export default RegisterPage;
