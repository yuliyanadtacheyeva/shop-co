import {
  AppBreadcrumbs,
  setupBreadcrumbsListener,
} from '../../components/app-breadcrumbs/app-breadcrumbs.ts';
import store from '../../redux/store.ts';
import { createCartCard } from '../../components/app-card-cart/app-card-cart.ts';
import { initializeCartCardHandlers } from '../../components/app-card-cart/app-card-cart-handlers.ts';
import { AppOrderSummary } from '../../components/app-order-summary/app-order-summary.ts';
import { initializeOrderSummaryHandlers } from '../../components/app-order-summary/app-order-summary-handlers';
import router from '../../router/router.ts';

const CartPage = async (): Promise<string> => {
  const breadcrumbs = AppBreadcrumbs('Cart');
  let pageContent = '';

  const state = store.getState().cart;
  const cartId = state.id;
  const isCartEmpty = state.products.length === 0;

  if (!cartId || isCartEmpty) {
    pageContent = `
      <div class="product-page-container mx-auto max-w-screen-xl p-4 md:p-8">
        <div id="breadcrumbs">${breadcrumbs}</div>
        <div class="flex justify-center align-center">
        <div class="text-xl border rounded-2xl max-w-4xl inline-block font-bold border-gray-300 p-8">Cart is empty.</div>
        </div>
      </div>
    `;
  } else {
    const cartCards = state.products.map(createCartCard).join('');
    const orderSummaryHtml = AppOrderSummary(
      state.total,
      state.total - state.discountedTotal,
      state.discountedTotal,
      'Go to Checkout',
      'cart',
      false,
    );

    pageContent = `
<div class="product-page-container mx-auto max-w-screen-xl p-4 md:p-8">
  <div id="breadcrumbs">${breadcrumbs}</div>
  <h2 class="text-start font-poppins font-bold text-4xl pb-12">Your Cart</h2>
  <div class="flex flex-col md:flex-row items-start">
    <div class="md:w-2/3 w-full m-2 bg-white shadow-lg rounded-lg">${cartCards}</div>
    <div class="md:w-1/3 w-full m-2">${orderSummaryHtml}</div>
  </div>
</div>
    `;
  }

  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = pageContent;
    setTimeout(() => {
      setupBreadcrumbsListener();
      initializeOrderSummaryHandlers('cart', (event: Event) => {
        event.preventDefault();
        router.navigate('/checkout');
      });
    }, 0);
  }

  state.products.forEach((product) => {
    initializeCartCardHandlers(product);
  });

  return pageContent;
};

export default CartPage;
