import { AppFormAuth } from '../../components/app-form-auth/app-form-auth.ts';
import { initializeAuthForm } from '../../components/app-form-auth/form-auth-handlers.ts';

const LoginPage = async () => {
  const pageContent = AppFormAuth('login');
  const mainElement = document.getElementById('routes');
  if (mainElement) {
    mainElement.innerHTML = pageContent;

    setTimeout(() => {
      initializeAuthForm();
    }, 0);
  }

  return pageContent;
};

export default LoginPage;
