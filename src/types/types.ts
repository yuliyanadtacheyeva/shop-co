export interface Product {
  id: number;
  title: string;
  description: string;
  price: number;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: string;
  thumbnail: string;
  images: string[];
}

export interface CartProduct {
  id: number;
  title?: string;
  price?: number;
  total?: number;
  discountPercentage?: number;
  quantity: number;
  thumbnail?: string;
}

export interface CartResponse {
  id: number;
  products: CartProduct[];
  total: number;
  discountedTotal: number;
  userId: number;
  totalProducts: number;
  totalQuantity: number;
  isDeleted?: boolean;
  deletedOn?: string;
}

export interface CartProductUpdate {
  id: number;
  quantity: number;
}
